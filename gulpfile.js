var gulp = require('gulp');
var inject = require('gulp-inject');
var clean = require('gulp-clean');
var sass = require('gulp-sass');
var cleanCSS = require('gulp-clean-css');
var minifyJs = require('gulp-minify');
var rename = require('gulp-rename');
var uglify = require('gulp-uglify');
var ngmin = require('gulp-ngmin');
var replace = require('gulp-replace-task');
var fs = require('fs');
var args = require('yargs').argv;

gulp.task('clean', function () {
  	// del('serve');
  	return gulp.src('serve')
  		.pipe(clean());
});

gulp.task('inject-libs', function() {

	var deps = [
	    './serve/assets/lib/angular.min.js',
	    './serve/assets/lib/angular-route.min.js',
	    './serve/assets/lib/angular-ui-router.min.js',
	    // './serve/assets/lib/ngStorage.min.js',
	    './serve/assets/lib/postfix.js',
	    './serve/assets/lib/moment.min.js',
	    // './serve/assets/lib/jquery.min.js',
	    // './serve/assets/lib/jquery-ui.min.js',
	    './serve/assets/lib/crypto-js.js',
	    './serve/assets/lib/modernizr',
	    './serve/assets/lib/*.js',
	    './serve/assets/css/*.css',
	    './serve/app/**/*',
	    './serve/components/**/*.js'
	];
	setTimeout(function() {
		return gulp.src('./src/index.html')
	    .pipe(inject(gulp.src(deps), { ignorePath: 'serve/', addRootSlash: false }))
	    .pipe(gulp.dest('./serve'));
	},1000);
	

});

gulp.task('copy-modules', function() {
  var jsLib = [
  	/*'./node_modules/jquery/dist/jquery.min.js',
  	'./node_modules/jquery-ui-dist/jquery-ui.min.js',*/
  	'./node_modules/angular-route/angular-route.min.js',
    './node_modules/angular/angular.min.js',
    './node_modules/angular-ui-router/release/angular-ui-router.min.js',
    // './node_modules/ng-storage/ngStorage.min.js',
    './node_modules/moment/min/moment.min.js',
    './node_modules/crypto-js/crypto-js.js',
    './bower_components/angular-post-fix/postfix.js',
    './node_modules/modernizr/bin/modernizr',
    './src/assets/lib/*.js'
  ];

   return gulp.src(jsLib)
  	.pipe(gulp.dest('./serve/assets/lib'));

});

gulp.task('copy-css', function() {
	var cssLib = [
	  	'./node_modules/bootstrap/dist/css/bootstrap.min.css'
	  ];
	
	return gulp.src(cssLib)
  	.pipe(gulp.dest('serve/assets/css'));

});

gulp.task('copy-components', function() {
	var includes = [
		// './src/components/**/*',
		'./src/components/**/*.html',
		'./src/assets/css/*.css',
		'./src/assets/lib/*.js',
		'./src/assets/image/*',
		'./src/app/min/**/*',
	];

	return gulp.src(includes, {base: './src'})
		.pipe(gulp.dest('./serve'));
});

gulp.task('compile-scss', function() {
	gulp.src('./src/assets/css/*.scss', {base: './src'})
		.pipe(sass.sync().on('error', sass.logError))
		.pipe(rename({ suffix: '.min'}))
		.pipe(cleanCSS({compatibility: 'ie8', keepBreaks: true}))
		.pipe(gulp.dest('./src'));
});

gulp.task('compile-constant-js', function() {
	var env = args.env || 'dev';
	var file = './src/config/constants.env.json';

	var settings = JSON.parse(fs.readFileSync(file,'utf8'));
  	var enVariables = {};

  	switch(env) {
		case 'test': enVariables = settings.test; break;
		case 'localTest': enVariables = settings.test; break;
		case 'uat': enVariables = settings.uat; break;
		case 'prod' : enVariables = settings.prod; break;
		default: enVariables = settings.dev;
	}

	return gulp.src('./src/app/raw/app.constants.js')
		.pipe(replace({
          patterns: [
            {
              match: 'saveToHistory',
              replacement: enVariables.saveToHistory
            },
            {
              match: 'mockedMovies',
              replacement: enVariables.mockedMovies
            },
            {
              match: 'getMovieHistory',
              replacement: enVariables.getMovieHistory
            }
          ]
	    }))
		.pipe(gulp.dest('./src/app'));
})

gulp.task('compile-app-js', function() {

	return gulp.src(['./src/app/*.js'])
		.pipe(ngmin({dynamic: true}))
		.pipe(uglify({mangle: false}))
		.pipe(rename({suffix: '.min'}))
    	.pipe(gulp.dest('./src/app/min'))
});

gulp.task('compile-ng-js', function() {
	return gulp.src('./src/components/**/*.js', {base:'./src'})
		.pipe(ngmin({dynamic: true}))
		.pipe(uglify({mangle: false}))
		.pipe(rename({ suffix: '.min'}))
		.pipe(gulp.dest('./serve'));
});

gulp.task('build', ['compile','copy','inject-libs']);
gulp.task('compile', ['compile-scss', 'compile-constant-js', 'compile-app-js', 'compile-ng-js']);
gulp.task('copy', ['copy-modules', 'copy-css', 'copy-components']);