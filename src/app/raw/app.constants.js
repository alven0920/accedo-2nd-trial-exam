angular.module('movie.on.constant', [])

.constant('saveToHistory', @@saveToHistory)
.constant('mockedMovies', @@mockedMovies)
.constant('getMovieHistory', @@getMovieHistory);