angular.module('movie.on.demand', [
	'movie.on.constant','httpPostFix',
	'ngRoute','ui.router',
	'movie.home.controller','movie.home.service',
	'movie.detail.controller', 'movie.detail.service',
	'player.pane.controller','player.pane.service',
	'user.history.controller', 'user.history.service',
	'angular-sly'
])

.config(function($stateProvider, $urlRouterProvider, getMovieHistory) {

	function keyPressHandler(event) {
		console.log(event);
	}

	$stateProvider
		.state('home', {
			url: '/home',
			templateUrl: 'components/movieHome/movieHomeView.html',
			controller: 'MovieHomeController',
			resolve: {
				promiseObj:  function($http){
		            return $http({method: 'GET', url: 'https://demo2697834.mockable.io/movies'});
		         }
			}
		})

		.state('history', {
			url: '/history/:userId',
			templateUrl: 'components/userHistory/userHistoryView.html',
			controller: 'UserHistoryController',
			resolve: {
				promiseObj:  function($http){
		            return $http({method: 'GET', url: getMovieHistory.url+'1'});
		         }
			}
		})

		.state('playNow', {
			url: '/nowplaying/:movieId',
			templateUrl: 'components/playerPane/playerPaneView.html',
			controller: 'PlayerPaneController',
			resolve: {
				promiseObj:  function($http){
		            return $http({method: 'GET', url: 'https://demo2697834.mockable.io/movies'});
		         }
			}
		});

		/*.state('movieDetail', {
			url: '/movieDetail/:movieId',
			templateUrl: 'components/movieDetails/movieDetailView.html',
			controller: 'MovieDetailController',
			resolve: {
				promiseObj:  function($http){
		            // $http returns a promise for the url data
		            return $http({method: 'GET', url: 'https://demo2697834.mockable.io/movies'});
		         }
			}
		});*/


	$urlRouterProvider.otherwise('/home');

});