angular.module('user.history.service', [])

.factory('UserHistoryService', function($http, $window, $q, getMovieHistory) {

	return {
		getWatched: function(userId) { // fetches from the server

			var userId = JSON.parse($window.sessionStorage.getItem('sessionId'));
			var dateToday = moment().format('YYMMDDhhmmss');
			
			if(!userId || userId === "") {
				userId = (CryptoJS.SHA1(CryptoJS.SHA1(dateToday.toString()))).toString();

				$window.sessionStorage.setItem('sessionId', JSON.stringify(userId.toString()));
			}

			var url = getMovieHistory.url+userId.toString();

			return promise = $http({
				'url' : url,
				'method' : 'GET',
			}).then(function(response) {
				if(response) {
					return response;
				}
			});
		}
	};

});