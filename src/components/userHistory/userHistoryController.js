angular.module('user.history.controller', [])

.controller('UserHistoryController', function($window, $scope, $state, $filter, MoviesService, UserHistoryService) {

	var moviesHistoryTemp = [];
	$scope.isNoData = false;

	var param = $state.params.userId || '1'; // session id
	var moviesList = JSON.parse($window.sessionStorage.getItem('moviesList'));
	var moviesCount = JSON.parse($window.sessionStorage.getItem('moviesCount'));
	
	
	if(moviesList && moviesCount) {
		$scope.movies = moviesList;
		$scope.count = moviesCount;
	} else {

		var promise = MoviesService.getMovies();
		promise.then(function(response) {

			if(response.data) {
				$scope.movies = response.data.entries;
				$scope.count = response.data.totalCount;

				$window.sessionStorage.setItem('moviesList', JSON.stringify(response.entries));
				$window.sessionStorage.setItem('moviesCount', JSON.stringify(response.totalCount));
			}
			
		});
	}

	var sessionId = JSON.parse($window.sessionStorage.getItem('sessionId'));

	if(!sessionId) {
		var dateToday = moment().format('YYMMDDhhmmss');

		sessionId = CryptoJS.SHA1(CryptoJS.SHA1(dateToday.toString()));
		$window.sessionStorage.setItem('sessionId', JSON.stringify(sessionId.toString()));
	}

	var histPromise = UserHistoryService.getWatched(sessionId);

	histPromise.then(function(response) {
		var data = response.data;

		if(data) { // check for any data
			var watched = [];

			/*if(!sessionId) { // get the session Id from the server and store in sessionStorage object
				//sessionId = data.sessionId;
				$window.sessionStorage.setItem('sessionId', JSON.stringify(data.sessionId));
			}*/
			
			if(data.moviesHistory.length > 0) {
				$scope.moviesHistory = data.moviesHistory;
				watched = data.moviesHistory;
				
				for(var i=0;i<watched.length;i++) {
					var result = $filter('filter')($scope.movies, {'id':watched[i].movie_id})[0];
					var item = {
						'id' : (i+1),
						'title' : result.title,
						'movieId' : watched[i].movie_id,
						'lastWatched' : watched[i].last_watched,
						'image' : result.images[0].url
					};
					moviesHistoryTemp.push(item);		
				}	
			} else { // no movie history yet
				$scope.isNoData = true;
				var item = {
					'id' : 1,
					'title' : 'No Recently Watched',
					'movieId' : 'no-history',
					'lastWatched' : '',
					'image' : ''
				};

				moviesHistoryTemp.push(item);	

			}

			$scope.moviesHistory = moviesHistoryTemp;
		}
	});

	$scope.goToWatchNow = function(movieId) {
		var promise = MoviesService.saveToHistory(movieId);
		$state.go('playNow', {'movieId': movieId});
	};
	
	$scope.returnToList = function() {
		$state.go('home');
	};

});