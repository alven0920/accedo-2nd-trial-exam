angular.module('player.pane.controller', [])

.controller('PlayerPaneController', function($window, $timeout, $sce, $scope, $state, $filter, MoviesService) {

	var movieId = $state.params.movieId;

	var moviesList = JSON.parse($window.sessionStorage.getItem('moviesList'));
	var moviesCount = JSON.parse($window.sessionStorage.getItem('moviesCount'));

	if(moviesList && moviesCount) {
		$scope.movies = moviesList;
		$scope.count = moviesCount;
	} else {
		var promise = MoviesService.getMovies();
		promise.then(function(response) {

			if(response.data) {
				$scope.movies = response.data.entries;
				$scope.count = response.data.totalCount;

				$window.sessionStorage.setItem('moviesList', JSON.stringify(response.entries));
				$window.sessionStorage.setItem('moviesCount', JSON.stringify(response.totalCount));
			}
			
		});
	}

	angular.element($window.document.getElementById('now-playing-vod')).on('timeupdate', function(e) {
		if(this.currentTime >= this.duration) {
			$timeout(function() {
				$state.go('home');
			}, 1000);
		}
	});

	$scope.movieDetail = $filter('filter')($scope.movies, {'id':movieId})[0];

	$scope.trustSrc = function(movieSrc) {
		return $sce.trustAsResourceUrl(movieSrc);
	}

	$scope.goToHistory = function() {
		var sessionId = JSON.parse($window.sessionStorage.getItem('sessionId'));
		$state.go('history', {'userId' : sessionId});
	}
	
	$scope.returnToList = function() {
		$state.go('home');
	};
});