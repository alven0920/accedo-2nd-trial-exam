angular.module('movie.home.service', [])

.factory('MoviesService', function($http, $q, $window, saveToHistory, mockedMovies) {
	/*
		var dateToday = moment().format('YYMMDD');
		var encryptedPass = CryptoJS.SHA1(CryptoJS.SHA1("info2013"+password)+dateToday.toString());
	*/
	return {
		saveToHistory: function(movieId, userId) {

			var data = {
				'movieId' : movieId,
				'userId' : userId,
				'lastWatched' : moment().format('YYYY-MM-DD hh:mm:ss')
			};

			return promise = $http({
				'url' : saveToHistory.url,
				'method' : 'POST',
				'data' : data
			});
		},
		getMovies: function() {
			var cache = undefined;
			var deferred = $q.defer();

			if(!cache) {
				$http({
					'url' : mockedMovies.url,
					'method' : 'GET'
				}).then(function(response) {
					cache = response.data;
					deferred.resolve(cache);
				});

				cache = deferred.promise;
				return $q.when(cache);

			} else {
				return;
			}
			
		}
	};

});