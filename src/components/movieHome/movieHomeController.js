angular.module('movie.home.controller', [])

.controller('MovieHomeController', function($window, $scope, $state, MoviesService) {

	if(!$scope.movies && !$scope.count) {
		var moviesList = JSON.parse($window.sessionStorage.getItem('moviesList'));
		var moviesCount = JSON.parse($window.sessionStorage.getItem('moviesCount'));
		
		if(moviesList && moviesCount) {
			$scope.movies = moviesList;
			$scope.count = moviesCount;
		} else {
			var promise = MoviesService.getMovies();
			promise.then(function(response) {
				$scope.movies = response.entries;
				$scope.count = response.totalCount;

				$window.sessionStorage.setItem('moviesList', JSON.stringify(response.entries));
				$window.sessionStorage.setItem('moviesCount', JSON.stringify(response.totalCount));
			});
		}
	}

	$scope.goToWatchNow = function(movieId) {

		var userId = JSON.parse($window.sessionStorage.getItem('sessionId'));

		if(!userId || userId === "") {
			var dateToday = moment().format('YYMMDDhhmmss');
			
			userId = (CryptoJS.SHA1(CryptoJS.SHA1(dateToday.toString()))).toString();
			

			$window.sessionStorage.setItem('sessionId', JSON.stringify(userId));
		}

		var promise = MoviesService.saveToHistory(movieId, userId);
		$state.go('playNow', {'movieId': movieId});
	};

	$scope.goToHistory = function() {
		var sessionId = JSON.parse($window.sessionStorage.getItem('sessionId'));
		$state.go('history', {'userId' : sessionId});
	}

    angular.element(document).ready(function(e) {
    	console.log('ready');
		document.getElementById('mov-0').focus();
	});

	angular.element($window.document.body).on('keyup', function(e) {
		e.preventDefault();
		e.stopPropagation();
		e.stopImmediatePropagation();
	
		var elem = $window.document.activeElement;

		if(e.key === "ArrowRight" || e.key === "ArrowLeft") {
			var tbIndex = elem.tabIndex;
			var npIndex = 0;

			if(e.key === "ArrowRight") {
				npIndex = tbIndex + 1;
			} else if(e.key === "ArrowLeft") {
				npIndex = tbIndex - 1;
			}

			//if(npIndex < 0 || npIndex > ($scope.count-1)) return;

			var adjElem = $window.document.getElementById('mov-'+npIndex);

			if(!adjElem) return;

			adjElem.focus();
			
		} else if (e.key === "Enter") {
			angular.element(elem).triggerHandler('click');
		}

		
		
	});

	angular.element($window.document.getElementsByClassName('mov-list')).on('wheel', function(event) {
		var start = event.pageX;
		var startPos = this.scrollLeft;

		var offset = start - event.pageX;
		
		if(event.deltaY > 0) {
			this.scrollLeft = startPos + event.deltaY;
		} else if(event.deltaY < 0) {
			this.scrollLeft = startPos + event.deltaY;
		}
	});

	angular.element(document.getElementsByClassName('mov-list')).on('mousedown', function(event) {
		var start = event.pageX;
		var startPos = this.scrollLeft;

		var elem = this;

		angular.element(elem).on('mousemove', function(event) {
			var offset = start - event.pageX;
			this.scrollLeft = startPos + offset;
			return false;
		});

 		angular.element(elem).on('mouseup', function(event) {
 			angular.element(elem).off('mousemove');
 		});
	});

});