angular.module('movie.detail.controller', [])

.controller('MovieDetailController', function($window, $scope, $state, $filter, MoviesService) {

	var movieId = $state.params.movieId;

	if(!$scope.movieDetail) {

		var moviesList = JSON.parse($window.sessionStorage.getItem('moviesList'));
		var moviesCount = JSON.parse($window.sessionStorage.getItem('moviesCount'));
		
		if(moviesList && moviesCount) {
			$scope.movieDetail = $filter('filter')(moviesList, {'id':movieId})[0];
		} else {
			var promise = MoviesService.getMovies();
			promise.then(function(response) {	
				moviesList = response.entries;

				$window.sessionStorage.setItem('moviesList', JSON.stringify(response.entries));
				$window.sessionStorage.setItem('moviesCount', JSON.stringify(response.totalCount));

				$scope.movieDetail = $filter('filter')(moviesList, {'id':movieId})[0];
			});
		}
	}

	$scope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams){
	    $window.document.getElementsByClassName('btn')[0].focus();
	});

	angular.element($window.document.getElementById('ui-view-pad')).on('keyup', function(e) {
		e.preventDefault();
		
		var elem = $window.document.activeElement;

		var npIndex = parseInt(elem.getAttribute('tabindex')); 

		if(e.key === "ArrowRight") {
			npIndex = npIndex + 1;
		} else if (e.key === "ArrowLeft") {
			npIndex = npIndex - 1;
		} else if (e.key === "Enter") {
			angular.element(elem).triggerHandler('click');
		}

		if(npIndex < 1 || npIndex > 2) return;

		var adjElem = $window.document.getElementById('btn-'+npIndex);
		adjElem.focus();

	});

	$scope.goToHistory = function() {
		var sessionId = JSON.parse($window.sessionStorage.getItem('sessionId'));
		$state.go('history', {'userId' : sessionId}, {'reload': true});
	}

	$scope.dateFormat = function() {
		var dateObj = new Date($scope.movieDetail.publishedDate);
		return moment(dateObj).format('MMMM D, YYYY');
	}

	$scope.goToWatchNow = function() {
		var promise = MoviesService.saveToHistory($scope.movieDetail.id);
		promise.then(function(response) {
			if(response.data) {
				var data = JSON.parse(response.data);
				$window.sessionStorage.setItem('sessionId', JSON.stringify(data));
			}
		});
		$state.go('playNow', {'movieId': movieId});
	};

	$scope.returnToList = function($event) {
		$state.transitionTo('home');
	}

});