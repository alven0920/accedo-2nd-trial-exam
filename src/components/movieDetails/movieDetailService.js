angular.module('movie.detail.service', [])

.factory('MovieDetailService', function($http, $window, $sessionStorage) {

	return {
		saveToHistory: function(movieId) {
			return promise = $http({
					'url' : 'http://localhost/vod_api/serve/AddMovieHistory',
					'method' : 'POST',
					'data' : {
						'movieId' : movieId,
						'userId' : '12341',
						'lastWatched' : moment().format('YYYY-MM-DD hh:mm:ss')
					}
				});
		}
	};

});